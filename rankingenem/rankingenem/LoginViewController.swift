//
//  ViewController.swift
//  rankingenem
//
//  Created by SAS on 05/02/16.
//  Copyright © 2016 Sistema Ari de Sá. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    static let url = "http://servico.portalsas.com.br:8080/api/GlobalAuth/genericlogin"
    
    let request = NSMutableURLRequest(URL: NSURL(fileURLWithPath: url))
    
    var autenticado = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        request.HTTPMethod = "POST"
        
        VerifyAccount()
    }

    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        return super.shouldPerformSegueWithIdentifier(identifier, sender: sender)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)
    }

    func VerifyAccount() {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

